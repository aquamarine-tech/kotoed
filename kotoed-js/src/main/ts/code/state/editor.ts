
export interface EditorState {
    fileName: string
    value: string
    loading: boolean
    diff: boolean
}
