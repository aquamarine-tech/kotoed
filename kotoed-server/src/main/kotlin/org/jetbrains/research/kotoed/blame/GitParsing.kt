package org.jetbrains.research.kotoed.blame

class GitParsing {

    private val diff = Regex("@@(.+\n)+")
    private val header = Regex("@@.+@@")
    private val old = Regex("((\n-.*)+\n)|(\n\\\\ No newline at end of file\n?)")
    private val startEnd = Regex("(^-.*\n)|(\n-.*)")

    fun getNew(context: String): String {
        val diff = getDiff(context)
        val diff2 =  diff.let { old.replace(it, "\n") }
        return diff2.let { startEnd.replace(it, "") }
    }

    fun getDiff(context: String): String {
        val diffStr = diff.find(context)?.value
        if (diffStr != null) {
            val delta = diffStr.let { header.replace(it, "") }
            return delta.substring(1, delta?.length-1)
        } else return ""
    }

}